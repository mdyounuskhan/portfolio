@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/experience') }}">Experience</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Experience</li>
                </ol>
            </nav>
            <div class="card">
                <div class="card-header text-white bg-success mb-3">
                    <h4>Edit Experience</h4>
                </div>
                <div class="card-body">
                    {{-- All Error Message Show Code --}}
                    {{-- @foreach ($errors->all() as $error)
                                {{ $error }}
                    @endforeach --}}
                    <form action="{{ url('edit/experience/insert') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Start Work</label>
                            <input type="hidden" name="id" value="{{ $Experience->id }}">
                            <input type="text" class="form-control @error('start_work') is-invalid @enderror" name="start_work"
                                value="{{ $Experience->start_work }}">
                            @error('start_work')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>End Work</label>
                            <input type="text" class="form-control @error('end_work') is-invalid @enderror" name="end_work"
                                value="{{ $Experience->end_work }}">
                            @error('end_work')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Employer Name</label>
                            <input type="text" class="form-control @error('employer_name') is-invalid @enderror" name="employer_name"
                                value="{{ $Experience->employer_name }}">
                            @error('employer_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Job Title</label>
                            <input type="text" class="form-control @error('job_Title') is-invalid @enderror" name="job_Title"
                                value="{{ $Experience->job_Title }}">
                            @error('job_Title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Job Description</label>
                            <textarea type="text" cols="30" rows="10" class="form-control @error('job_description') is-invalid @enderror" name="job_description">{{ $Experience->job_description }}</textarea>
                            @error('job_description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success">Edit Experience</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
