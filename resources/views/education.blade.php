@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>University Name</th>
                        <th>Degree</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>About</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <td>{{ $education->university_name }}</td>
                    <td>{{ $education->degree }}</td>
                    <td>{{ $education->start_date }}</td>
                    <td>{{ $education->end_date }}</td>
                    <td>{{ $education->about }}</td>
                    <td>
                        <a href="{{ url('edit/education') }}/{{ $education->id }}">
                            <div class="btn btn-info">Edit</div>
                        </a>
                    </td>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
