@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Start Work</th>
                        <th>End Work</th>
                        <th>Employer Name</th>
                        <th>Jobs Title</th>
                        <th>Jobs Description</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <td>{{ $Experience->start_work }}</td>
                    <td>{{ $Experience->end_work }}</td>
                    <td>{{ $Experience->employer_name }}</td>
                    <td>{{ $Experience->job_Title }}</td>
                    <td>{{ $Experience->job_description }}</td>
                    <td>
                        <a href="{{ url('edit/experience') }}/{{ $Experience->id }}">
                            <div class="btn btn-info">Edit</div>
                        </a>
                    </td>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
