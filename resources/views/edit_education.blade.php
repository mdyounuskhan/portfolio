@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/education') }}">Education</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Education</li>
                </ol>
            </nav>
            <div class="card">
                <div class="card-header text-white bg-success mb-3">
                    <h4>Edit Education</h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('edit/education/insert') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>university Name</label>
                            <input type="hidden" name="id" value="{{ $education->id }}">
                            <input type="text" class="form-control @error('university_name') is-invalid @enderror"
                                name="university_name" value="{{ $education->university_name }}">
                            @error('university_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Degree</label>
                            <input type="text" class="form-control @error('degree') is-invalid @enderror"
                                name="degree" value="{{ $education->degree }}">
                            @error('degree')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Start Date</label>
                            <input type="text" class="form-control @error('start_date') is-invalid @enderror"
                                name="start_date" value="{{ $education->start_date }}">
                            @error('start_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>End Date</label>
                            <input type="text" class="form-control @error('end_date') is-invalid @enderror"
                                name="end_date" value="{{ $education->end_date }}">
                            @error('end_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>About</label>
                            <input type="text" class="form-control @error('about') is-invalid @enderror"
                                name="about" value="{{ $education->about }}">
                            @error('about')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success">Edit Education</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
