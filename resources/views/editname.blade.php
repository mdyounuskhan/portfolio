@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/name') }}">Name</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Name</li>
                </ol>
            </nav>
            <div class="card">
                <div class="card-header text-white bg-success mb-3">
                    <h4>Edit Name</h4>
                </div>
                <div class="card-body">
                    {{-- All Error Message Show Code --}}
                    {{-- @foreach ($errors->all() as $error)
                                {{ $error }}
                    @endforeach --}}
                    <form action="{{ url('edit/name/insert') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="hidden" name="id" value="{{ $name->id }}">
                            <input type="text" class="form-control @error('name') is-invalid @enderror"
                                name="name" value="{{ $name->name }}">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Title</label>
                            <input type="text" class="form-control @error('title') is-invalid @enderror"
                                name="title" value="{{ $name->title }}">
                            @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success">Edit Name</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
