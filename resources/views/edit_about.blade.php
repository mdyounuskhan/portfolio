@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ url('/about') }}">About</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Edit About</li>
                </ol>
            </nav>
            <div class="card">
                <div class="card-header text-white bg-success mb-3">
                    <h4>Edit Name</h4>
                </div>
                <div class="card-body">
                    {{-- All Error Message Show Code --}}
                    {{-- @foreach ($errors->all() as $error)
                                {{ $error }}
                    @endforeach --}}
                    <form action="{{ url('edit/about/insert') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>About</label>
                            <input type="hidden" name="id" value="{{ $about->id }}">
                            <textarea type="text" class="form-control @error('about') is-invalid @enderror" cols="30" rows="10" name="about">{{ $about->about }}</textarea>
                            @error('about')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success">Edit About</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
