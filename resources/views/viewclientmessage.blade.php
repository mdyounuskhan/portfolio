@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Email</th>
                        <th>Message</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($messages as $message)
                    <tr class="{{ ($message->status == 1)? "bg-secondary":"" }}">
                        <td>{{ $message->email }}</td>
                        <td>{{ $message->message }}</td>
                        <td>
                            @if ($message->status == 1)
                            <a href="{{ url('read/status') }}/{{ $message->id }}"><button class="btn btn-sm btn-success">UNREAD</button></a>
                            @else
                                <button class="btn btn-sm btn-success">READ DONE</button>
                            @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
