@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>About</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <td>{{ $about->about }}</td>
                    <td>
                        <a href="{{ url('edit/about') }}/{{ $about->id }}">
                            <div class="btn btn-info">Edit</div>
                        </a>
                    </td>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
