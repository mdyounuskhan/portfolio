@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($skills as $skill)
                    <tr>
                        <td>{{ $skill->name }}</td>
                        <td>
                            <a href="{{ url('delete/skill') }}/{{ $skill->id }}">
                                <div class="btn btn-danger">Delete</div>
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="col-6">
            <div class="card">
                <div class="card-header text-white bg-success mb-3">
                    <h4>Add Skill</h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('add/skill') }}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success">Add Skill</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
