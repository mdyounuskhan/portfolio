@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-4">
            <div class="card">
                <div class="card-header text-white bg-success mb-3">
                    <h4>Add Project</h4>
                </div>
                <div class="card-body">
                    <form action="{{ url('add/project') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>Name</label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name">
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>About</label>
                            <textarea name="about" class="form-control @error('about') is-invalid @enderror" rows="6"></textarea>
                            @error('about')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Photo</label>
                            <input type="file" class="form-control @error('photo') is-invalid @enderror" name="photo">
                            @error('photo')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-success">Add Project</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-8">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>About</th>
                        <th>Photo</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($projects as $project)
                    <tr>
                    <td>{{ $project->name }}</td>
                    <td>{{ $project->about }}</td>
                    <td>
                        <img src="{{ asset('uploads/project_images/'.$project->photo) }}" width="80" alt="Not Found">
                    </td>
                    <td>
                        <a href="{{ url('delete/project') }}/{{ $project->id }}"><button class="btn btn-sm btn-danger">Delete</button></a>
                    </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
