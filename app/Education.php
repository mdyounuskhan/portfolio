<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
    protected $fillable = ['university_name', 'degree', 'start_date', 'end_date', 'about'];
}
