<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    protected $fillable = ['start_work', 'end_work', 'employer_name', 'job_Title', 'job_description'];
}
