<?php

namespace App\Http\Controllers;

use App\About;
use App\Skill;
use App\Education;
use App\Experience;
use App\Message;
use App\Name;
use App\Project;
use App\Setting;
use Carbon\Carbon;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    function index()
    {
        $settings = Setting::all();
        $skills = Skill::all();
        $projects = Project::all();
        $Experience = Experience::find(1);
        $about = About::find(1);
        $name = Name::find(1);
        $education = Education::find(1);
        return view('welcome', compact('name', 'about', 'Experience', 'education', 'projects', 'skills', 'settings'));
    }
    //Client Message
    function clientmessage(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'message' => 'required'
        ]);

        Message::insert([
            'email' => $request->email,
            'message' => $request->message,
            'status' => 1,
            'created_at' => Carbon::now()
        ]);
        return back()->with('status', 'Message Sent Successfully');
    }
    function view()
    {
        $messages = Message::all();
        return view('viewclientmessage', compact('messages'));
    }
    function readstatus($message_id)
    {
        Message::find($message_id)->update([
            'status' => 2
        ]);
        return back();
    }
}
