<?php

namespace App\Http\Controllers;

use App\About;
use App\Education;
use App\Experience;
use App\Message;
use App\Name;
use App\Project;
use App\Setting;
use App\Skill;
use Carbon\Carbon;
use Image;
use Illuminate\Http\Request;

class NameController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //Name
    function index()
    {
        $name = Name::find(1);
        return view('name', compact('name'));
    }
    function editname($name_id)
    {
        $name = Name::find($name_id);
        return view('editname', compact('name'));
    }
    function editnameinsert(Request $req)
    {
        $req->validate([
            'name' => 'required',
            'title' => 'required'
        ]);
        Name::find($req->id)->update([
            'name' => $req->name,
            'title' => $req->title,
        ]);
        return back();
    }

    //About
    function about()
    {
        $about = About::find(1);
        return view('about', compact('about'));
    }
    function editabout($about_id)
    {
        $about = About::find($about_id);
        return view('edit_about', compact('about'));
    }
    function editaboutinsert(Request $request)
    {
        $request->validate([
            'about' => 'required'
        ]);

        About::find($request->id)->update([
            'about' => $request->about,
        ]);
        return back();
    }
    //Experience
    function experience()
    {
        $Experience = Experience::find(1);
        return view('experience', compact('Experience'));
    }
    function editexperience($experience_id)
    {
        $Experience = Experience::find($experience_id);
        return view('edit_experience', compact('Experience'));
    }
    function editexperienceinsert(Request $request)
    {
        $request->validate([
            'start_work' => 'required',
            'end_work' => 'required',
            'employer_name' => 'required',
            'job_Title' => 'required',
            'job_description' => 'required'
        ]);

        Experience::find($request->id)->update([
            'start_work' => $request->start_work,
            'end_work' => $request->end_work,
            'employer_name' => $request->employer_name,
            'job_Title' => $request->job_Title,
            'job_description' => $request->job_description
            ]);
            return back();
    }
    //Education
    function education()
    {
        $education = Education::find(1);
    return view('education', compact('education'));
    }
    function editeducation($education_id)
    {
        $education = Education::find($education_id);
        return view('edit_education', compact('education'));
    }
    function editeducationinsert(Request $request)
    {
        $request->validate([
            'university_name' => 'required',
            'degree' => 'required',
            'start_date' => 'required',
            'end_date' => 'required',
            'about' => 'required'
        ]);

        Education::find($request->id)->update([
            'university_name' => $request->university_name,
            'degree' => $request->degree,
            'start_date' => $request->start_date,
            'end_date' => $request->end_date,
            'about' => $request->about
        ]);
        return back();
    }
    //Project
    function project()
    {
        $projects = Project::all();
        return view('project', compact('projects'));
    }
    function addproject(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'about' => 'required'
        ]);

        $insert_id = Project::insertGetId([
            'name' => $request->name,
            'about' => $request->about,
            'created_at' => Carbon::now()
        ]);
        if ($request->hasFile('photo')) {
            $main_photo = $request->photo;
            $photo_name = $insert_id.".". $main_photo->getClientOriginalExtension();
            Image::make($main_photo)->resize(300, 300)->save(base_path('public/uploads/project_images/' . $photo_name));
            Project::find($insert_id)->update([
                'photo'=> $photo_name
            ]);
        }
        return back();
    }
    function deleteproject($project_id)
    {
        Project::find($project_id)->delete();
        return back();
    }
    //Skill
    function skill()
    {
        $skills = Skill::all();
        return view('skill', compact('skills'));
    }
    function addskill(Request $request)
    {
        Skill::insert([
            'name' => $request->name,
            'created_at' => Carbon::now()
        ]);
        return back();
    }
    function deleteskill($skill_id)
    {
        Skill::find($skill_id)->delete();
        return back();
    }
    //Settings
    function settings()
    {
        $settings = Setting::all();
        return view('settings', compact('settings'));
    }
    function addsettings(Request $request)
    {
        Setting::insert([
            'name' => $request->name,
            'link' => $request->link,
            'created_at' => Carbon::now()
        ]);
        return back();
    }
    function deletesetting($setting_id)
    {
        Setting::find($setting_id)->delete();
        return back();
    }
}
