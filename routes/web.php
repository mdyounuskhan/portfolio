<?php

Route::get('/', 'FrontendController@index');

Auth::routes([
    'register' => false, // Registration Routes...
    // 'reset' => false, // Password Reset Routes...
    // 'verify' => false, // Email Verification Routes...
]);

Route::get('/home', 'HomeController@index')->name('home');
//Name
Route::get('/name', 'NameController@index');
Route::get('/edit/name/{name_id}', 'NameController@editname');
Route::post('/edit/name/insert', 'NameController@editnameinsert');
//About
Route::get('/about', 'NameController@about');
Route::get('/edit/about/{about_id}', 'NameController@editabout');
Route::post('/edit/about/insert', 'NameController@editaboutinsert');
//Experience
Route::get('/experience', 'NameController@experience');
Route::get('/edit/experience/{experience_id}', 'NameController@editexperience');
Route::post('/edit/experience/insert', 'NameController@editexperienceinsert');
//Education
Route::get('/education', 'NameController@education');
Route::get('/edit/education/{education_id}', 'NameController@editeducation');
Route::post('/edit/education/insert', 'NameController@editeducationinsert');
//Project
Route::get('/project', 'NameController@project');
Route::post('/add/project', 'NameController@addproject');
Route::get('/delete/project/{project_id}', 'NameController@deleteproject');
//Skill
Route::get('/skill', 'NameController@skill');
Route::post('/add/skill', 'NameController@addskill');
Route::get('/delete/skill/{skill_id}', 'NameController@deleteskill');
//Client Message
Route::post('/client/message', 'FrontendController@clientmessage');
Route::get('/view/client/message', 'FrontendController@view');
Route::get('/read/status/{message_id}', 'FrontendController@readstatus');
//Settings
Route::get('/settings', 'NameController@settings');
Route::post('/add/settings', 'NameController@addsettings');
Route::get('/delete/setting/{setting_id}', 'NameController@deletesetting');




